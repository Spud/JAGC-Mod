# JAGC Mod

Borrowed from Gruppe Adler!
Gruppe Adler Mod is a collection of Gruppe Adler original addons, that add new functionality or modify and improve existing functionality to suit our Arma gameplay.

All credits go to Gruppe Adler!

## For Developers

### Releasing

Versioning is handled by `addons\main\script_mod.hpp`, which is automatically updated by our build script.

### Building

Use `tools/build.sh` to create a packed version of the mod in `\release`.

#### Windows

Open the Git Shell from Github for Windows, and type `bash tools\build.sh`

#### Linux

Execute `tools/build.sh`.

### Adding stuff

Only when adding new files to the *root directory* you will have to edit `build.sh` to include it into the builds.
