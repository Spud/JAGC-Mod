class RscText;
class RscButtonMenu;
class RscListbox;
class RscCheckBox;
class RscControlsGroup;
class RscControlsGroupNoScrollbars;

class RscStructuredText {
    class Attributes;
};

class RscDisplayAttributes {
    class Controls {
        class Background;
        class Title;
        class Content: RscControlsGroup {
            class controls;
        };
        class ButtonOK;
        class ButtonCancel;
    };
};
