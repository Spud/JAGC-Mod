### zeus
Adds new modules to the curator interface. Modules can be found in the category "Gruppe Adler".

#### Maintainer(s)
* chris579 / Jörgn
* McDiod

#### Supply Drop
This module lets you paradrop supply containers with planes.

1. Fill supply container
2. Drop module on container
3. Choose any plane
4. Choose destination

#### Monitoring
Provides zeuses with additional information, displayed 3D on relevant objects.

* player FPS
* player medical status
* object locality
* AI status
* summary window, giving totals and averages
