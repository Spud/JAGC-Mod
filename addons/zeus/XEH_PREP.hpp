PREP(moduleDiagnosticsGetAIStatus);
PREP(moduleDiagnosticsGetMedicalStatus);
PREP(moduleDiagnosticsGetOwnerName);
PREP(moduleDiagnosticsHandleDisconnect);
PREP(moduleDiagnosticsOnConfirm);
PREP(moduleDiagnosticsRender);
PREP(moduleDiagnosticsServerToggle);
PREP(moduleDiagnosticsUpdateFPS);
PREP(moduleDiagnosticsUpdateObjectLocality);

PREP(ui_moduleDiagnostics);
PREP(ui_moduleDiagnosticsActivateSummaryWindow);
PREP(ui_moduleDiagnosticsUpdateSummaryWindow);

PREP(moduleSupplyDrop);
PREP(moduleSupplyDropExit);
PREP(moduleSupplyDropSuccess);
